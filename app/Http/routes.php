<?php

/**
 * return welcome view
 */
Route::get('/', function () {
    return view('welcome');
});

/**
 * return all articles with condition
 */
Route::get('/return-content', function () {
    $articles = \App\Article::where('created_at', '<', '2017-07-12')->orderBy('id', 'desc')->take(3)->get();
    return view('returned')->with('articles', $articles);
});

/**
 * return all without conditions
 */
Route::get('/return-all', function () {
    $articles = \App\Article::all();
    return view('all')->with('articles', $articles);
});

/**
 * soft delete article using id
 */
Route::get('/soft-delete/{id}', function ($id) {
    \App\Article::find($id)->delete();
    return redirect('/return-all');
});

/**
 * return all soft deleted articles
 */
Route::get('/trash', function () {
    $articles = \App\Article::onlyTrashed()->get();
    return view('trash')->with('articles', $articles);
});

/**
 * restore soft deleted article using id
 */
Route::get('/restore-deleted/{id}', function ($id) {
    \App\Article::onlyTrashed()->find($id)->restore();
    return redirect('/return-all');
});

/**
 * delete soft deleted article form database
 */
Route::get('/force-deleted/{id}', function ($id) {
    \App\Article::onlyTrashed()->find($id)->forceDelete();
    return redirect('/trash');
});