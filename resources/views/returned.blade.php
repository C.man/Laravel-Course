<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Returned Data</title>
    </head>
    <body>
        @if(count($articles))

            <article>

            @foreach($articles as $article)

                <h3>{{ $article->title }}</h3>
                <p>Published At: {{ $article->created_at }}</p>
                    <hr>
                    <br>
                <p>{{ $article->content }}</p>
                    <hr>
                    <hr>
                    <br>
            @endforeach

            </article>

        @endif
    </body>
</html>
